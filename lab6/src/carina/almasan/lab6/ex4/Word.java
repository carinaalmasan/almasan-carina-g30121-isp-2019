package carina.almasan.lab6.ex4;

public class Word {
    public String name;
    public Word(String name) {
        this.name = name;
    }
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof  Word)) return false;
        Word w=(Word)o;
        return name.equals(w.name);
    }
    public int hashCode(){
        return (int)(name.length()*1000);
    }
}
