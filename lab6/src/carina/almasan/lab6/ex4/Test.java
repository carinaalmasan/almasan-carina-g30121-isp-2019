package carina.almasan.lab6.ex4;

import java.io.*;
public class Test{
    public static void main(String[] args) throws IOException {

        Dictionary dict = new Dictionary();
        char raspuns;
        String linie;
        String explic;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("Menu");
            System.out.println("a - Add word");
            System.out.println("s - Search word");
            System.out.println("w - Show words");
            System.out.println("d - Show definitions");
            System.out.println("l - Show dictionary");
            System.out.println("e - Exit");

            linie = fluxIn.readLine();
            raspuns = linie.charAt(0);

            switch (raspuns) {
                case 'a': case 'A':
                    System.out.println("Write a word:");
                    linie = fluxIn.readLine();
                    if (linie.length() > 1) {
                        System.out.println("Write the definition:");
                        explic = fluxIn.readLine();
                        dict.addWord(new Word(linie), new Definition(explic));
                    }
                    break;
                case 's': case 'S':
                    System.out.println("The word you were looking for is:");
                    linie = fluxIn.readLine();
                    if (linie.length() > 1) {
                        Word x = new Word(linie);
                        explic = String.valueOf(dict.getDefinition(x));

                        if (explic == null)
                            System.out.println("The word couldn't be fount");
                        else
                            System.out.println("Definition:" + explic);
                    }
                    break;
                case 'l': case 'L':
                    System.out.println("Print:");
                    dict.afisDictionar();
                    break;
                case 'w': case 'W':
                    System.out.println("Print all words");
                    dict.getAllWords();
                    break;
                case 'd': case 'D':
                    System.out.println("Print all definitions");
                    dict.getAllDefinitions();
                    break;

            }
        } while (raspuns != 'e' && raspuns != 'E');
        System.out.println("Programm finished");



    }

}

