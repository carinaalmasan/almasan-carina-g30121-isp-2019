package carina.almasan.lab6.ex2;

public class Test {

    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Serban",190);
        bank.addAccount("Mihai",70);
        bank.addAccount("Andrei",100);
        bank.addAccount("Ionut",80);
        bank.addAccount("Alex",110);
        System.out.println("Print Accounts by balance");
        bank.printAccounts();
        System.out.println("Print Accounts between limits");
        bank.printAccounts(80,110);
        System.out.println("Get Account by owner name");
        bank.getAccount("Ionut");
        System.out.println("Get All Account order by name");
        bank.getAllAccount();

    }
}

