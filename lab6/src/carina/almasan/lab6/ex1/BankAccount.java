package carina.almasan.lab6.ex1;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance)
    {
        this.balance=balance;
        this.owner=owner;
    }
    public void withdraw(double amount) {
        this.balance = this.balance-amount;
    }

    public void deposit(double amount) {
        this.balance = this.balance-amount;
    }

    public boolean equals(Object o) {
        if (o instanceof BankAccount) {
            BankAccount b = (BankAccount) o;
            return balance == b.balance && b.owner.equals(owner);
        }
        return false;
    }
    public int hashCode() {
        return (int)balance + owner.hashCode();
    }
}

