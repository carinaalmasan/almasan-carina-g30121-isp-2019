package carina.almasan.lab6.ex3;

public class Test {

    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Serban",1900);
        bank.addAccount("Mihai",700);
        bank.addAccount("Andrei",1000);
        bank.addAccount("Ionut",800);
        bank.addAccount("Alex",1100);


        System.out.println("Print Accounts by balance");
        bank.printAccounts();

        System.out.println("Print Accounts between limits");
        bank.printAccounts(800,1100);

        System.out.println("Get Account by owner name");
        bank.getAccount("Serban",1900);

        System.out.println("Get All Account order by name");
        bank.getAllAccount();

    }
}
