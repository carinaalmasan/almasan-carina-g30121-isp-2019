package carina.almasan.lab9.ex2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Counter{
    JButton plus=new JButton("plus");
    JButton minus=new JButton("minus");
    JFrame frm=new JFrame();
    JTextField text=new JTextField(15);
    int i=0;
    public Counter(){
        text.setText(" " + i);
        frm.setVisible(true);
        frm.setSize(200, 100);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setResizable(true);
        frm.setLayout(new FlowLayout());
        frm.add(text);
        frm.add(plus);
        frm.add(minus);
        plus.addActionListener(this::ActionPerformed);
        minus.addActionListener(this::ActionPerformed);
    }
    public void ActionPerformed(ActionEvent e){
        if ((e.getSource()==plus)&&(e.getSource()!=minus))
            i++;

        else
            i--;
        text.setText(""+i);
    }

    public static void main(String[] args) {
        new Counter();
    }

}

