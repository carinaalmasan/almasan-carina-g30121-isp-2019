package carina.almasan.lab9.ex3;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.io.*;

public class ReadFile extends JFrame {

    JButton button;
    JTextField text;
    JTextField inputText;
    JLabel label1;

    ReadFile() {
        setTitle("File reader");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300, 350);
        setVisible(true);
    }


    public void init() {
        this.setLayout(null);
        int width = 160;
        int height = 60;

        label1 = new JLabel();
        label1.setBounds(20, 10, width, height);
        label1.setText("FileName");

        inputText = new JTextField();
        inputText.setBounds(20, 60, width, height);

        text = new JTextField();
        text.setBounds(20, 160, width, height);

        button = new JButton("ReadFile");
        button.setBounds(20, 260, width, height);
        button.setSize(100, 20);
        button.addActionListener(new A());

        add(button);
        add(text);
        add(inputText);
        add(label1);

    }

    public static void main(String[] args) {
        new ReadFile();
    }

    class A implements ActionListener {

        private String filePath = "C:\\Users\\cari\\Documents\\almasan-carina\\lab9\\src\\Ex3\\";

        @Override
        public void actionPerformed(ActionEvent e) {

            inputText.setText("");
            try {
                Scanner in = new Scanner(new File(text.getText()));
                while (in.hasNextLine()) {
                }

            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }

        }
    }
}


