package carina.almasan.lab10.ex6;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Timer implements ActionListener {
    static int milisec = 0;
    static int sec = 0;
    static int min = 0;
    static int ore = 0;
    JLabel oree, minn, secc, milisecc;
    JButton start, stop, reset;
    private JFrame frame;
    private JPanel panel = new JPanel ();
    static boolean state = true;

    Timer () {
        frame = new JFrame ("Cronometru");
        frame.add (panel);
        frame.setDefaultCloseOperation (WindowConstants.EXIT_ON_CLOSE);
        frame.setSize (400, 200);
        frame.setLocation (400, 400);
        panel.setLayout (null);
        init ();
        frame.setVisible (true);
    }

    public void init () {
        int width = 60;
        int height = 20;
        oree = new JLabel ("00:");

        oree.setBounds (20, 20, width, height);
        panel.add (oree);


        minn = new JLabel ("00:");
        minn.setBounds (60, 20, width, height);
        panel.add (minn);


        secc = new JLabel ("00:");
        secc.setBounds (100, 20, width, height);
        panel.add (secc);


        milisecc = new JLabel ("00");
        milisecc.setBounds (140, 20, width, height);
        panel.add (milisecc);


        start = new JButton ("Start");
        start.setBounds (20, 50, 70, 20);
        panel.add (start);
        stop = new JButton ("Stop");
        stop.setBounds (100, 50, 70, 20);
        panel.add (stop);
        reset = new JButton ("Reset");
        reset.setBounds (180, 50, 70, 20);
        panel.add (reset);
        start.addActionListener (this);
        stop.addActionListener (this);
        reset.addActionListener (this);


    }

    public void actionPerformed (ActionEvent e) {
        if (e.getSource () == start) {
            state = true;
            Thread t = new Thread () {
                public void run () {

                    for (; ; ) {
                        if (state == true) {
                            try {
                                Thread.sleep (1);
                                milisec++;
                                if (milisec > 634) {
                                    milisec = 0;
                                    sec++;
                                }
                                if (sec > 59) {
                                    sec = 0;
                                    min++;
                                }
                                if (min > 59) {
                                    min = 0;
                                    ore++;
                                }
                                milisecc.setText (":" + milisec);
                                secc.setText (":" + sec);
                                minn.setText (":" + min);
                                oree.setText ("" + ore);

                            } catch (Exception e) {
                            }
                        } else {
                            break;
                        }
                    }
                }
            };
            t.start ();
        }
        if (e.getSource () == stop) {
            state = false;
        }
        if (e.getSource () == reset) {

            milisec = 0;
            sec = 0;
            min = 0;
            ore = 0;
            milisecc.setText ("00");
            secc.setText ("00:");
            minn.setText ("00:");
            oree.setText ("00:");

        }
    }

    public static void main (String[] args) {
        new Timer ();
    }

}