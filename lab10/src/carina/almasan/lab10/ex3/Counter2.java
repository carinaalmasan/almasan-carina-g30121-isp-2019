package carina.almasan.lab10.ex3;


public class Counter2 extends Thread {
    Counter2(String name){
        super(name);
    }
    public void run(){
        for(int i=0;i<10;i++){
            System.out.println(getName() + " i = "+i);
            try {
                Thread.sleep((int)(Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }
    public void run2(){
        for(int i=10;i<20;i++){
            System.out.println(getName() + " i = "+i);
            try {
                Thread.sleep((int)(Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }
    public static void main(String[] args) {
        Counter2 c1 = new Counter2("counter1");
        Counter2 c2 = new Counter2("counter2");

        c1.run();
        c2.run2();
    }
}
