package carina.almasan.lab4.ex3;
import carina.almasan.lab4.ex3.Book;


public class Test {
    public static void main(String [] args){
        Author aAuthor=new Author("almasan","email@gmail.com", 'f');
        System.out.println(aAuthor);
        Book aBook=new Book ("b", aAuthor,45,78);
        System.out.println(aBook);
        System.out.println("name is: " + aBook.getName());
        System.out.println("price is: " + aBook.getPrice());
        System.out.println("qty is: " + aBook.getQtyInStock());
        System.out.println("Author is: "+ aBook.getAuthor());
        System.out.println("Author s name is: " + aBook.getAuthor().getName());
        System.out.println("Author s email is: " + aBook.getAuthor().getEmail());



    }
}
