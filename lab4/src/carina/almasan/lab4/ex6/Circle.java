package carina.almasan.lab4.ex6;
import carina.almasan.lab4.ex6.Shape;

public class Circle extends Shape {
    private double radius;
    Circle(){
        radius=1.0;
    }
    Circle(double radius ){
        this.radius=radius;
    }
    Circle(double radius, String color, boolean filled){
        super(color,filled);
        this.radius=radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    public double getArea(){
        return radius*radius*Math.PI;
    }
    public double getPerimeter(){
        return 2*radius*Math.PI;
    }

    @Override
    public String toString() {
        return "Circle{" + "radius=" + radius + '}';
    }
}
