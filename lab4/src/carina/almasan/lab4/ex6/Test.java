package carina.almasan.lab4.ex6;
import carina.almasan.lab4.ex6.Shape;
import carina.almasan.lab4.ex6.Rectangle;
import carina.almasan.lab4.ex6.Circle;
import carina.almasan.lab4.ex6.Square;

public class Test {
    public static void main(String[] args){
        Shape s1=new Circle(4,"yellow",true);
        System.out.println(s1);
        System.out.println(s1.getColor());
        System.out.println(s1.isFilled());

        Circle c1=(Circle)s1;
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getColor());
        System.out.println(c1.isFilled());
        System.out.println(c1.getRadius());

        Shape s2=new Rectangle(1.0,2.0,"green",false);
        System.out.println(s2);
        System.out.println((s2.getColor()));
        Rectangle r1= (Rectangle)s2;
        System.out.println(r1);
        System.out.println(r1.getArea());
        System.out.println(r1.getLength());
        System.out.println(r1.getColor());

        Shape s3 = new Square(6.7);
        System.out.println(s3);
        System.out.println(s3.getColor());

        Rectangle r2 = (Rectangle)s3;
        System.out.println(r2);
        System.out.println(r2.getArea());
        System.out.println(r2.getColor());

        System.out.println(r2.getLength());


        Square sq1 = (Square)r2;
        System.out.println(sq1);
        System.out.println(sq1.getArea());
        System.out.println(sq1.getColor());
        System.out.println(sq1.getSide());
        System.out.println(sq1.getLength());
    }
}
