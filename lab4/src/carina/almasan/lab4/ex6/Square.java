package carina.almasan.lab4.ex6;
import carina.almasan.lab4.ex6.Rectangle;


public class Square extends Rectangle{
    private double side;
    Square(double side){
        super(side, side);
    }
    Square(double side, String color, boolean filled){
        super(side, side, color, filled);
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    public void setWidth(double size){
        super.setWidth(side);
    }
    public void setLength(double side){
        super.setLength(side);
    }

    @Override
    public String toString() {
        return "Square{" + "side=" + side + '}';
    }
}
