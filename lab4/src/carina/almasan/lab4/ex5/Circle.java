package carina.almasan.lab4.ex5;

public class Circle {
    private double radius;
    private String color;
    public Circle(){
        this.radius=1.0;
        this.color="red";
    }
    Circle(double r,String c ){
        radius=r;
        color=c;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return 2 * Math.PI * radius;
    }
}
