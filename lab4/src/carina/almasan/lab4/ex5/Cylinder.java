package carina.almasan.lab4.ex5;
import carina.almasan.lab4.ex5.Circle;

public class Cylinder extends Circle{
    double height;
    Cylinder(double rad, String c, double height){
        super(rad,c);
        this.height=height;
    }

    public double getHeight() {
        return height;
    }
    public double getRadius() {
        return super.getRadius();
    }

    public double getArea() {
        return 2 * Math.PI * height*getRadius()+2*Math.PI*getRadius()*(getRadius()+height);

    }
}

