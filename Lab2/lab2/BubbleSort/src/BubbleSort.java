package almasan.carina.Lab2.ex5;


import java.util.Scanner;

public class BubbleSort {
    public static void main(String[] args) {
        int n, i, max;
        Scanner in = new Scanner(System.in);
        System.out.print("Introduceti dimensiunea vectorului =  ");
        n = in.nextInt();
        int[] v = new int[n];
        for (i = 0; i < n; i++) {
            System.out.print("Introduceti elementul " + i + ": ");
            v[i] = in.nextInt();
            System.out.println();
        }
        int sortat;
        do {
            sortat = 1;
            for (i = 0; i < n - 1; i++) {
                if (v[i] > v[i + 1]) {
                    int aux = v[i];
                    v[i] = v[i + 1];
                    v[i + 1] = aux;
                    sortat = 0;
                }
            }
        } while (sortat == 0);

        System.out.print("Vectorul sortat este : ");
        System.out.println();

        for (i = 0; i < n; i++) {
            System.out.print(" Elementul " + i + ": " + v[i]);
            System.out.println();
        }
    }
}
