package almasan.carina.lab2.e4;
import java.util.Scanner;
public class MaxVector {
    public static void main(String[] args) {
        int a,i,max;
        Scanner in = new Scanner(System.in);
        System.out.print ("dimensiunea vectorului =  ");
        a = in.nextInt();
        int[] v=new int[a];
        for(i=0;i<a;i++) {
            System.out.print ("a[" +i+ "]:");
            v[i] = in.nextInt();
        }
        max=v[0];
        for(i=1;i<a;i++)
        {
            if(v[i]>max)
                max=v[i];
        }
        System.out.print ("Max este " +max);
    }
}
