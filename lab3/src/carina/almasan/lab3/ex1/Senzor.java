package carina.almasan.lab3.ex1;

public class Senzor {
    public int x;
    public Senzor(){
        this.x=-1;
    }
    public void change(int k){
        this.x=k;
    }
    public int tooString(){
        return this.x;
    }
}
