package carina.almasan.lab3.ex4;

public class MyPoint {
    public int x;
    public int y;

    public MyPoint(){
        this.x=0;
        this.y=0;
    }
    public MyPoint(int x, int y){
        this.x=x;
        this.y=y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    public String toString() {
        return "(" +  x + "," + y + ')';
    }
    public double distance (int x, int y){
        return Math.sqrt((x-this.x)*(x-this.x)+(y-this.y)*(y-this.y));
    }
    public double distance(MyPoint a){
        return Math.sqrt((a.x-this.x)*(a.x-this.x)+(a.y-this.y)*(a.y-this.y));
    }
    public void setxy(int x, int y){
        this.x=x;
        this.y=y;
    }
}

