package carina.almasan.lab3.ex2;

public class TestCircle {
    public static void main(String [] args){
        Circle c1=new Circle();
        System.out.println(c1.getRadius());
        Circle c2=new Circle(6.0);
        System.out.println(c2.getArea());
        Circle c3=new Circle(5.0, "green");
        System.out.println(c3.getArea());
        System.out.println(c3.getRadius());
    }
}

