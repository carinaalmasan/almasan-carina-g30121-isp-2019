package carina.almasan.lab5.ex2;

public class ProxyImage implements Image {

    private Image realImage;
    private Image rotatedImage;
    private String fileName;

    public ProxyImage(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void display() {
        if (realImage == null) {
            realImage = new RealImage(fileName);
        }
        realImage.display();

        if (rotatedImage == null) {
            rotatedImage = new RotatedImage(fileName);
        }
        rotatedImage.display();
    }
}

