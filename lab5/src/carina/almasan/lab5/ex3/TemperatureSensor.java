package carina.almasan.lab5.ex3;

import java.util.Random;

public class TemperatureSensor extends Sensor{

    @Override
    public int readValue() {
        Random r = new Random();
        return r.nextInt(100);
    }

    @Override
    public String getLocation() {
        return null;
    }
}



