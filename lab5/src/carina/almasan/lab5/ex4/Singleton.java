package carina.almasan.lab5.ex4;
import carina.almasan.lab5.ex3.LightSensor;
import carina.almasan.lab5.ex3.TemperatureSensor;

public class Singleton {
    private Singleton(){

    }
    private static Singleton Controller;
    public static void control() throws InterruptedException {


        TemperatureSensor ts= new TemperatureSensor();
        LightSensor ls = new LightSensor();


        int sec = 1;
        while (sec <=10)
        {
            System.out.println("Temp: " + ts.readValue());
            System.out.println("Light: " + ls.readValue());
            System.out.println("Sec: " + sec);
            sec++;
            Thread.sleep(1000);
        }


    }


    public static void main(String[] args) throws InterruptedException {

        Controller.control();

    }
}

