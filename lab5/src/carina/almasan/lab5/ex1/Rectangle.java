package carina.almasan.lab5.ex1;
import carina.almasan.lab5.ex1.Shape;

public class Rectangle extends Shape{
    double width;
    double length;
    public Rectangle(double width, double length){
        this.width=width;
        this.length=length;
    }
   public Rectangle(double width, double length, String color, boolean filled){
        super(color,filled);
        this.width=width;
        this.length=length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
    public double getArea() {
        return  this.length*this.width;
    }
    public double getPerimeter() {
        return 2*(this.length+this.width);
    }

    public String toString() {
        return "Rectangle{" + "width=" + width + ", length=" + length + '}';
    }
}
