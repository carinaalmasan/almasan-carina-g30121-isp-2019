package carina.almasan.lab5.ex1;
import carina.almasan.lab5.ex1.Rectangle;

public class Square extends Rectangle {
    double side;

    public Square(double side,String color, boolean filled){
       super(side,side,color,filled);
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    public void setWidth(double side) {
        super.width = side;
    }

    public void setLength(double side) {
        super.length = side;
    }

    @Override
    public String toString() {
        return "Square{" + "side=" + side + '}';
    }
}
