package carina.almasan.lab5.ex1;
import carina.almasan.lab5.ex1.Shape;

public class Circle extends Shape {
    double radius;
    Circle(double radius, String blue, String aTrue){
        this.radius=radius;
    }
    Circle(double radius, String color, boolean filled){
       super(color,filled);
        this.filled=filled;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return  Math.PI * radius * radius;
    }
    public double getPerimeter(){
        return 2*Math.PI*radius;
    }

    public String toString() {
        return "Circle{" + "radius=" + radius + '}';
    }
}

