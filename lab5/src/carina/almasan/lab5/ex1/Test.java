package carina.almasan.lab5.ex1;

public class Test {
    public static void main(String [] args){
        Circle c1=new Circle(1,"blue","true");
        System.out.println(c1.getPerimeter());
        System.out.println(c1.getArea());
        c1.toString();
        System.out.println();

        Rectangle r1=new Rectangle(10.0, 20.0, "red", true);
        System.out.println(r1.getArea());
        System.out.println(r1.getPerimeter());
        r1.toString();
        System.out.println();

        Square s1=new Square(5,"blue", false );
        System.out.println(s1.getArea());
        System.out.println(s1.getPerimeter());
        s1.toString();
        System.out.println();


    }
}
