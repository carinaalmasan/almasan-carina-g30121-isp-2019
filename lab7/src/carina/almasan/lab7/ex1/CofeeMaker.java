package carina.almasan.lab7.ex1;

public class CofeeMaker {

    private static int coffeNumber;

    Cofee makeCofee () throws MaxCoffeNumber {

        System.out.println("Make a coffe");
        int t = (int) (Math.random() * 100);
        int c = (int) (Math.random() * 100);
        Cofee cofee = new Cofee(t, c);

        coffeNumber++;
        if(coffeNumber>14) throw new MaxCoffeNumber("Can't create more coffes");

        return cofee;

    }

}
