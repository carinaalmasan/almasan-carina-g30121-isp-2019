package carina.almasan.lab8.ex4;

import java.util.ArrayList;
import java.util.Scanner;

public class ControlUnit {


    //declaring multiple fireSensors and one tempSensor
    private ArrayList<FireSensor> fireSensors = new ArrayList<FireSensor>();
    private TemperatureSensor temperatureSensor;
    private static String owner;
    private static int desiredTemperature;


    //singleton implementation

    private ControlUnit() {};
    private static ControlUnit controlUnit;

    public static ControlUnit getInstance() {
        if (controlUnit == null) {
            controlUnit = new ControlUnit();
            Scanner scan = new Scanner(System.in);

            System.out.println("Enter house owner: ");
            owner = scan.nextLine();
            System.out.println("Enter desired temperature: ");
            desiredTemperature = scan.nextInt();
        }


        return controlUnit;
    }







    //setting the owner and temperature




    public void setDesiredTemperature(int desiredTemperature) {
        this.desiredTemperature = desiredTemperature;
    }


    public static void analyseEvent(Event event)
    {
        getInstance();
        if(event.getType().equals(EventType.TEMPERATURE))
        {
            if(((TemperatureEvent)event).getVlaue() > desiredTemperature)
            {
                CoolingUnit c = new CoolingUnit();
                c.cooling(desiredTemperature);
            }else if(((TemperatureEvent)event).getVlaue() < desiredTemperature)
            {
                HeatingUnit h = new HeatingUnit();
                h.heating(desiredTemperature);
            }

        } else if(event.getType().equals(EventType.FIRE))
        {
            AlarmUnit alarm = new AlarmUnit(owner);
            try {
                alarm.event(event);
            }catch (Exception e)
            {
                System.out.println("Fire event depleted \n");
            }
        }

    }




}